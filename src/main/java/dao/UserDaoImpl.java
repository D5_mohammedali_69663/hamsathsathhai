package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pojos.User;

import static utils.DBUtils.getConn;

public class UserDaoImpl implements UserDao {
	
	private Connection cn;
	private PreparedStatement pst1;

	// ctor
	public UserDaoImpl() throws SQLException {
		// get cn from db utils
		cn = getConn();
		// pst1
		pst1 = cn.prepareStatement("select * from users where email=? and password=?");
		System.out.println("user dao created!");

	}

	@Override
	public User authenticateUser(String email, String pwd) throws SQLException {
		// set IN params
		pst1.setString(1, email);
		pst1.setString(2, pwd);
		try (ResultSet rst = pst1.executeQuery()) {
			System.out.println("in user dao : exec query");
			if (rst.next())
//				int userId, String fullName, String email, String password, String phoneNo, Date date
				return new User(rst.getInt(1), rst.getString(2), email, pwd, rst.getString(5), rst.getDate(6));
		}
		return null;
	}
	
	// clean up dao
	public void cleanUp() throws SQLException {
		if (pst1 != null)
			pst1.close();
		System.out.println("user dao cleaned up!");
	}

}
