package pojos;

import java.sql.Date;

public class Blog {
	
	private int id;
	private String title;
	private String content;
	private Date createdDate;
	private int userId;
	private int categoryId;
	
	public Blog() {
	}

	public Blog(int id, String title, String content, Date createdDate, int userId, int categoryId) {
		this.id = id;
		this.title = title;
		this.content = content;
		this.createdDate = createdDate;
		this.userId = userId;
		this.categoryId = categoryId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	@Override
	public String toString() {
		return "Blog [id=" + id + ", title=" + title + ", content=" + content + ", createdDate=" + createdDate
				+ ", userId=" + userId + ", categoryId=" + categoryId + "]";
	}
	
	

}
